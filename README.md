# Project Scraper

Ingest content Application.

## Getting started

### Clone Project

```
git clone -v https://gitlab.com/gardo984/project_scraper.git
git remote update origin -p
git checkout main
```

### Installation

The project requires `Poetry` package manager, in order to continue the installation, please follow the next steps:
- Download and install poetry : https://python-poetry.org/docs/#osx--linux--bashonwindows-install-instructions
- Once installed, go to the project directory and activate the virtual env:
```sh
poetry shell
```
- Proceed to install the packages:
```sh
poetry install
```
- **Optional** commands:
	- Display virtual env directory:
	```sh
	poetry env info --path
	```
	- Add a new package:
	```sh
	poetry add <package-name>
	```
	- Show all packages:
	```sh
	poetry show
	```

### Usage

- To run the project locally:
```sh
flask run
```
- Test app:
```sh
curl http://localhost:8001/
curl http://localhost:8001/authors/peru
curl http://localhost:8001/authors/countries
```

### Image Build

- Build image:
```sh
docker image build -f backend/Dockerfile -t project_scraper:1.0 .
docker image tag project_scraper:1.0 malazo/project_scraper:latest
```
- Push image:
```sh
docker push malazo/project_scraper:latest
```
- To run image locally:
```sh
docker run --name app_scraper  -d -p 8001:8001  malazo/project_scraper:latest
```
- To pull
```sh
docker pull malazo/project_scraper
```

## Description
Project that ingest content from Wikipedia related to Authors

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

