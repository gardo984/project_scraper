from requests import request
from typing import (
    List,
    Dict,
    Any,
    NoReturn,
    Sequence,
)
from bs4 import BeautifulSoup


class AuthorScraper(object):

    def __init__(self, url: str):
        if not url:
            raise ValueError('Error url not specified')
        self.url = url
        self.scraper = None

    def ingest_content(self) -> NoReturn:
        rsp = request('get', url=self.url)
        if not rsp.status_code == 200:
            raise ValueError('Error getting url content')
        self.scraper = BeautifulSoup(rsp.text, 'html.parser')

    def normalize_authors_list(self, items: Sequence[str]) -> List[Dict]:
        """ Normalize list of ingested authors """
        items = list(set(items))
        normalized_list = []
        for item in items:
            split = item.split('(')
            name = split[0]
            live_range = (
                '({}'.format(split[1]) if len(split) == 2 else '-'
            )
            data = {'name': name, 'live_range': live_range, }
            normalized_list.append(data)
        return normalized_list

    def get_peruvian_authors(self) -> List[str]:
        if not self.scraper:
            return []
        authors = self.normalize_authors_list([
            ele.text.split(',')[0].strip()
            for ele in self.scraper.find(
                'div', class_="mw-parser-output"
            ).find('ul').find_all('li')
        ])
        return authors

    def get_author_countries(self) -> List[str]:
        if not self.scraper:
            return []
        div_tag = (
            self.scraper.find(
                'span', {'id': "Lists_by_ethnicity_or_nationality"}
            ).parent.next_sibling.next_sibling.next_sibling
        )
        if not div_tag:
            raise ValueError("Error, country container tag doesn't exist")
        countries = [
            ele.text.replace('writers', '').strip()
            for ele in div_tag.find('ul').find_all('li')
        ]
        return countries
