from .views import (
    IngestAuthorCountriesResource,
    IngestPeruvianAuthorsResource,
    AuthorCountriesResource,
    PeruvianAuthorsResource,
    WelcomeAll,
)
