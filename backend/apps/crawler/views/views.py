from flask_restful import Resource
from apps.crawler.helpers import AuthorScraper
from database.models import (
    db,
    AuthorsCountry,
    Authors,
)
from database.serializers import (
    AuthorsSchema,
    AuthorsCountrySchema,
)

PERUVIAN_AUTHORS_URL: str = "https://en.wikipedia.org/wiki/List_of_Peruvian_writers"  # noqa
AUTHOR_COUNTRIES_URL: str = "https://en.wikipedia.org/wiki/Lists_of_writers"
API_VERSION = 1.0


class WelcomeAll(Resource):
    def get(self):
        return {
            'msg': f'Welcome to Authors API interface {API_VERSION}'
        }


class PeruvianAuthorsResource(Resource):
    """
    Display the list of Peruvian Authors
    """

    def get(self):
        rows = Authors.query.all()
        items = AuthorsSchema().dump(rows, many=True)
        return {
            'data': items,
            'count': len(items),
        }


class AuthorCountriesResource(Resource):

    def get(self):
        rows = AuthorsCountry.query.all()
        items = AuthorsCountrySchema().dump(
            rows, many=True
        )
        return {
            'data': items,
            'count': len(items),
        }


class IngestAuthorCountriesResource(Resource):
    def get(self):
        scraper = AuthorScraper(url=AUTHOR_COUNTRIES_URL)
        scraper.ingest_content()
        items = scraper.get_author_countries()
        AuthorsCountry.remove_all()
        instances = [
            AuthorsCountry(country=c) for c in items
        ]
        db.session.add_all(instances)
        db.session.commit()
        return {
            'msg': 'Author Countries ingested successfully.',
            'count': len(items),
        }


class IngestPeruvianAuthorsResource(Resource):

    def get(self):
        scraper = AuthorScraper(url=PERUVIAN_AUTHORS_URL)
        scraper.ingest_content()
        items = scraper.get_peruvian_authors()
        instances = [Authors(**instance) for instance in items]
        Authors.remove_all()
        db.session.add_all(instances)
        db.session.commit()
        return {
            'msg': 'Authors ingested successfully.',
            'count': len(items),
        }
