

from settings.db import db


class Base(db.Model):
    __abstract__ = True

    created = db.Column(
        db.DateTime,
        default=db.func.now(),
    )
    updated = db.Column(
        db.DateTime,
        default=db.func.now(),
        onupdate=db.func.now(),
    )

    @classmethod
    def remove_all(cls) -> None:
        """
        Clean table before new content is added.
        """
        for c in cls.query.all():
            db.session.delete(c)
            db.session.commit()


class Authors(Base):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(
        db.String(120),
        unique=False,
        nullable=False,
    )
    live_range = db.Column(
        db.String(25),
        unique=False,
        nullable=False,
    )

    def __init__(self, name, live_range):
        self.name = name
        self.live_range = live_range

    def __repr__(self):
        return '<Authors {name} {live_range}>'.format(
            name=self.name,
            live_range=self.live_range
        )


class AuthorsCountry(Base):
    id = db.Column(db.Integer, primary_key=True)
    country = db.Column(
        db.String(80),
        unique=True,
        nullable=False,
    )

    def __repr__(self):
        return '<AuthorsCountry %r>' % self.country
