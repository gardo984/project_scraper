
from marshmallow import (
    Schema,
    fields,
)
DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'


class BaseSchema(Schema):
    created = fields.DateTime(format=DATETIME_FORMAT)
    updated = fields.DateTime(format=DATETIME_FORMAT)


class AuthorsSchema(BaseSchema):
    name = fields.Str()
    live_range = fields.Str()


class AuthorsCountrySchema(BaseSchema):
    country = fields.Str()
