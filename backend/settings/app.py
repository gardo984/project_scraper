
from flask_restful import (
    Api,
    Resource,
)
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from apps.crawler.views import (
    PeruvianAuthorsResource,
    AuthorCountriesResource,
    WelcomeAll,
    IngestAuthorCountriesResource,
    IngestPeruvianAuthorsResource,
)
from settings.utils import (
    create_app,
)
from database.models import *


app = create_app()
api = Api(app)
SQLAlchemy(app)
migrate = Migrate(app, db)


api.add_resource(WelcomeAll, "/")
api.add_resource(PeruvianAuthorsResource, "/authors/peru")
api.add_resource(AuthorCountriesResource, "/authors/countries")
api.add_resource(IngestPeruvianAuthorsResource, "/ingest/authors/peru")
api.add_resource(IngestAuthorCountriesResource, "/ingest/authors/countries")
