import os
from typing import List
from flask import Flask


def configure_app(app: Flask) -> None:
    app.config['SQLALCHEMY_DATABASE_URI'] = get_postgres_connection()
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


def create_app() -> Flask:
    app = Flask(__name__)
    configure_app(app)
    return app


def get_postgres_connection() -> str:
    data = os.environ
    required_fields: List = [
        'DATABASE_USERNAME',
        'DATABASE_PASSWORD',
        'DATABASE_HOST',
        'DATABASE_PORT',
        'DATABASE_NAME',
    ]
    match_condition = (
        all([data.get(x) for x in required_fields])
    )
    if not match_condition:
        raise SystemError('DB variables not found.')

    connection_str = (
        'postgresql://{username}:{password}@{host}:{port}/{database}'
        .format(
            username=os.environ.get('DATABASE_USERNAME'),
            password=os.environ.get('DATABASE_PASSWORD'),
            host=os.environ.get('DATABASE_HOST'),
            port=os.environ.get('DATABASE_PORT'),
            database=os.environ.get('DATABASE_NAME'),
        )
    )
    return connection_str
