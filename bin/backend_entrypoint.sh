#!/bin/bash
# *-* encoding : UTF-8 *-*

source /root/.bashrc
arg="$1"
if [ $arg == "setup" ]; then
	flask db upgrade
	python -m flask run
elif [ $arg == "debug" ]; then
	exec /bin/bash
else
	exec "$@"
fi